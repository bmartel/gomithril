package main

import (
	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
	"github.com/rs/cors"
)

func main() {
	e := echo.New()

	// Middleware
	e.Use(mw.Logger())
	e.Use(mw.Recover())
	e.Use(mw.Gzip())
	e.Use(cors.Default().Handler)

	// Serve Assets
	e.Static("/assets", "client/build/assets")
	e.ServeFile("/*", "client/build/index.html")

	// Start server
	e.Run(":8080")
}
