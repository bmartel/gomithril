# Go + Mithril <3 #

# How do I get set up?

## Get the project
* `git clone https://bmartel@bitbucket.org/bmartel/gomithril.git`

## From the Client Directory
* `npm install`
* `npm run build`

## Install GO and dependency manager
* Install GO https://golang.org/
* Ensure GO vendor experimental features are enabled for your system environment set `export GO15VENDOREXPERIMENT=1` in shell profile (.zshrc, .bashrc, .profile etc.)
* `go get -u github.com/FiloSottile/gvt`

## Install project dependencies
* `gvt rebuild`

## Build frontend assets binary
* `go-bindata-assetfs client/build/...`

## Build app binary
* `go build`

## Run the app (Run from $PROJECT_ROOT)
* `./gomithril`

## Visit http:localhost:1323 and enjoy running a self contained web server that can be deployed and run anywhere.
