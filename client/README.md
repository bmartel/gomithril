# mithril-redux-starter-webpack

# including modules

* [mithril](https://github.com/lhorie/mithril.js)
* [redux](https://github.com/rackt/redux) with [redux-logger](https://github.com/fcomb/redux-logger) and [redux-thunk](https://github.com/gaearon/redux-thunk)
* [webpack](https://npmjs.com/package/webpack)
* [ud](https://github.com/AgentME/ud)
* [sass](http://sass-lang.com) with [bourbon](http://bourbon.io/) + [neat](http://neat.bourbon.io/)

# quick start

```
$ npm run watch
```
and navigate to http://localhost:9000

# commands

* `npm run build` - build js for production
* `npm run watch` - automatically build js on file changes for development
