package main

import (
	"net/http"
	"path/filepath"

	"github.com/elazarl/go-bindata-assetfs"
	"github.com/labstack/echo"
)

// Index serves index file.
func Index(e *echo.Echo, file string) {
	ServeFile(e, "/", file)
}

// Favicon serves the default favicon - GET /favicon.ico.
func Favicon(e *echo.Echo, file string) {
	ServeFile(e, "/favicon.ico", file)
}

// Static serves static files from a directory. It's an alias for `Echo.ServeDir`
func Static(e *echo.Echo, path, dir string) {
	ServeDir(e, path, dir)
}

// ServeDir serves files from a directory.
func ServeDir(e *echo.Echo, path, dir string) {
	e.Get(path+"*", func(c *echo.Context) error {
		return ServeAsset(dir, c.P(0), c)
	})
}

// ServeFile serves a file.
func ServeFile(e *echo.Echo, path, file string) {
	e.Get(path, func(c *echo.Context) error {
		dir, file := filepath.Split(file)
		return ServeAsset(dir, file, c)
	})
}

func ServeAsset(dir, file string, c *echo.Context) error {
	fs := &assetfs.AssetFS{Asset, AssetDir, dir}
	f, err := fs.Open(file)
	if err != nil {
		return echo.NewHTTPError(http.StatusNotFound)
	}
	defer f.Close()

	fi, _ := f.Stat()
	if fi.IsDir() {
		file = filepath.Join(file, "index.html")
		f, err = fs.Open(file)
		if err != nil {
			return echo.NewHTTPError(http.StatusForbidden)

		}
		fi, _ = f.Stat()
	}

	http.ServeContent(c.Response(), c.Request(), fi.Name(), fi.ModTime(), f)
	return nil
}
